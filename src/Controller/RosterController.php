<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Personnage;
use App\Entity\Classe;
use App\Entity\Specialisation;

class RosterController extends AbstractController
{
    /**
     * @Route("/roster", name="roster")
     */
    public function index()
    {
        /**** MEMBRE ROSTER ****/
        $tankRoster = $this->getDoctrine()->getRepository(Personnage::class)->getRosterByRole('tank');
        $healRoster = $this->getDoctrine()->getRepository(Personnage::class)->getRosterByRole('heal');
        $cacRoster = $this->getDoctrine()->getRepository(Personnage::class)->getRosterByRole('cac');
        $castRoster = $this->getDoctrine()->getRepository(Personnage::class)->getRosterByRole('cast');

        /**** MEMBRE REROLL ****/
        $tankReroll = $this->getDoctrine()->getRepository(Personnage::class)->getRerollByRole('tank');
        $healReroll = $this->getDoctrine()->getRepository(Personnage::class)->getRerollByRole('heal');
        $cacReroll = $this->getDoctrine()->getRepository(Personnage::class)->getRerollByRole('cac');
        $castReroll = $this->getDoctrine()->getRepository(Personnage::class)->getRerollByRole('cast');

        /**** STATS ****/
        $roster = $this->getDoctrine()->getRepository(Personnage::class)->findBy(['inRoster' => true]);

        $totalMembre = count($roster);
        $ilvlMoy = $lvlCollierMoy = $lvlCapeMoy = 0;
        foreach ($roster as $perso) {
            $ilvlMoy += $perso->getIlevel();
            $lvlCollierMoy += $perso->getLevelCollier();
            $lvlCapeMoy += $perso->getLevelCape();
        }
        $ilvlMoy = $ilvlMoy / $totalMembre;
        $lvlCollierMoy = $lvlCollierMoy/ $totalMembre;
        $lvlCapeMoy = $lvlCapeMoy/ $totalMembre;
        /**** DATA POUR SELECT ****/
        $classes = $this->getDoctrine()->getRepository(Classe::class)->findAll();
        $specialisations = $this->getDoctrine()->getRepository(Specialisation::class)->findAll();

        return $this->render('roster/index.html.twig', [
            'tankRoster' => $tankRoster,
            'healRoster' => $healRoster,
            'cacRoster' => $cacRoster,
            'castRoster' => $castRoster,
            'tankReroll' => $tankReroll,
            'healReroll' => $healReroll,
            'cacReroll' => $cacReroll,
            'castReroll' => $castReroll,
            'totalMembre' => $totalMembre,
            'ilvlMoy' => $ilvlMoy,
            'lvlCollierMoy' => $lvlCollierMoy,
            'lvlCapeMoy' => $lvlCapeMoy,
            'classes' => $classes,
            'specialisations' => $specialisations,
        ]);
    }
}
