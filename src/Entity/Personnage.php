<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonnageRepository")
 */
class Personnage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pseudo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $inRoster;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Joueur", inversedBy="personnages")
     */
    private $joueur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $royaume;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Classe", inversedBy="personnages")
     */
    private $classe;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Specialisation", inversedBy="personnages")
     */
    private $spe1;

    /**
     * @ORM\Column(type="integer")
     */
    private $ilevel;

    /**
     * @ORM\Column(type="integer")
     */
    private $levelCollier;

    /**
     * @ORM\Column(type="integer")
     */
    private $levelCape;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getInRoster(): ?bool
    {
        return $this->inRoster;
    }

    public function setInRoster(bool $inRoster): self
    {
        $this->inRoster = $inRoster;

        return $this;
    }

    public function getJoueur(): ?Joueur
    {
        return $this->joueur;
    }

    public function setJoueur(?Joueur $joueur): self
    {
        $this->joueur = $joueur;

        return $this;
    }

    public function getRoyaume(): ?string
    {
        return $this->royaume;
    }

    public function setRoyaume(string $royaume): self
    {
        $this->royaume = $royaume;

        return $this;
    }

    public function getClasse(): ?Classe
    {
        return $this->classe;
    }

    public function setClasse(?Classe $classe): self
    {
        $this->classe = $classe;

        return $this;
    }

    public function getSpe1(): ?Specialisation
    {
        return $this->spe1;
    }

    public function setSpe1(?Specialisation $spe1): self
    {
        $this->spe1 = $spe1;

        return $this;
    }

    public function getIlevel(): ?int
    {
        return $this->ilevel;
    }

    public function setIlevel(int $ilevel): self
    {
        $this->ilevel = $ilevel;

        return $this;
    }

    public function getLevelCollier(): ?int
    {
        return $this->levelCollier;
    }

    public function setLevelCollier(int $levelCollier): self
    {
        $this->levelCollier = $levelCollier;

        return $this;
    }

    public function getLevelCape(): ?int
    {
        return $this->levelCape;
    }

    public function setLevelCape(int $levelCape): self
    {
        $this->levelCape = $levelCape;

        return $this;
    }
}
