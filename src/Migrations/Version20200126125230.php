<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200126125230 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE classe (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, color VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE specialisation (id INT AUTO_INCREMENT NOT NULL, classe_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, role VARCHAR(255) NOT NULL, INDEX IDX_B9D6A3A28F5EA509 (classe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE specialisation ADD CONSTRAINT FK_B9D6A3A28F5EA509 FOREIGN KEY (classe_id) REFERENCES classe (id)');
        $this->addSql('ALTER TABLE personnage ADD classe_id INT DEFAULT NULL, ADD spe1_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE personnage ADD CONSTRAINT FK_6AEA486D8F5EA509 FOREIGN KEY (classe_id) REFERENCES classe (id)');
        $this->addSql('ALTER TABLE personnage ADD CONSTRAINT FK_6AEA486DCB8B63D2 FOREIGN KEY (spe1_id) REFERENCES specialisation (id)');
        $this->addSql('CREATE INDEX IDX_6AEA486D8F5EA509 ON personnage (classe_id)');
        $this->addSql('CREATE INDEX IDX_6AEA486DCB8B63D2 ON personnage (spe1_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE personnage DROP FOREIGN KEY FK_6AEA486D8F5EA509');
        $this->addSql('ALTER TABLE specialisation DROP FOREIGN KEY FK_B9D6A3A28F5EA509');
        $this->addSql('ALTER TABLE personnage DROP FOREIGN KEY FK_6AEA486DCB8B63D2');
        $this->addSql('DROP TABLE classe');
        $this->addSql('DROP TABLE specialisation');
        $this->addSql('DROP INDEX IDX_6AEA486D8F5EA509 ON personnage');
        $this->addSql('DROP INDEX IDX_6AEA486DCB8B63D2 ON personnage');
        $this->addSql('ALTER TABLE personnage DROP classe_id, DROP spe1_id');
    }
}
