<?php

namespace App\Repository;

use App\Entity\Personnage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Personnage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Personnage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Personnage[]    findAll()
 * @method Personnage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonnageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Personnage::class);
    }

    public function getRosterByRole($role)
    {
        return $this->createQueryBuilder('p')
            ->join('p.spe1', 's')
            ->andWhere('p.inRoster = 1')
            ->andWhere('s.role = :role')
            ->setParameter('role', $role)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getRerollByRole($role)
    {
        return $this->createQueryBuilder('p')
            ->join('p.spe1', 's')
            ->andWhere('p.inRoster = 0')
            ->andWhere('s.role = :role')
            ->setParameter('role', $role)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }
}
